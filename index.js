const fs = require("fs");
const { server, app } = require("./generateServer")();
const { generateTextToSpeech, validateGCPTTSConfig, validatePollyTTSConfig } = require("./textToSpeech");

app.get('/health', (req, res) => {

  validateGCPTTSConfig()
  .then(() => {
      return validatePollyTTSConfig();
  }, err => {
    console.log(err);
    return res.status(503).send('GCP TTS DOWN');
  })
  .then(() => {
    return res.status(200).send('OK')
  }, (err => {
    console.log(err);
    return res.status(503).send('AWS POLLY TTS DOWN');
  }))
})

app.post("/", (req, res) => {
  const {
    speakersProfile,
    speakerNumber,
    langCode,
    text,
    outputFormat,
    audioSpeed,
  } = req.body;
  console.log("RECEIVED RQUEST");
  console.log(req.body);

  generateTextToSpeech({
    speakersProfile,
    speakerNumber,
    langCode,
    text,
    audioSpeed,
    outputFormat,
  })
    .then((filePath) => {
      res.sendFile(filePath, (err) => {
        if (err) {
          console.log("error sending back file", err);
        }
        fs.unlink(filePath, () => {});
      });
    })
    .catch((err) => {
      console.log("error generating text to speech", err);
      return res.status(400).send("Something went wrong");
    });
});

const PORT = process.env.PORT || 4000;
server.listen(PORT);
console.log(`Magic happens on port ${PORT}`); // shoutout to the user
console.log(`==== Running in ${process.env.NODE_ENV} mode ===`);
exports = module.exports = app; // expose app
